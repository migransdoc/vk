# delete groups in which I am admin

import vk_api


def captcha_handler(captcha):
    """ При возникновении капчи вызывается эта функция и ей передается объект
        капчи. Через метод get_url можно получить ссылку на изображение.
        Через метод try_again можно попытаться отправить запрос с кодом капчи
    """

    key = input("Enter captcha code {0}: ".format(captcha.get_url())).strip()

    # Пробуем снова отправить запрос с капчей
    return captcha.try_again(key)


def main():
    login, password = '', ''
    vk_session = vk_api.VkApi(login, password, captcha_handler=captcha_handler)
    try: vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    vk = vk_session.get_api()

    groups_admin = vk.groups.get(filter='admin')  # all groups where i am an admin
    members = list(map(lambda x: x[:-1], list(open('members.txt', 'r').readlines())))

    for group in groups_admin['items']:
        # vk.groups.editManager(group_id=group, user_id=125585197)  # Shirokov is not an admin of group
        # vk.groups.ban(group_id=group, owner_id=125585197)
        vk.groups.leave(group_id=group)
        '''for member in members:
            if member == '202812518':
                continue
            vk.groups.ban(group_id=group, owner_id=member)'''
        print('done ban one group')
    print('done Shirokov and ban everybody')


if __name__ == '__main__':
    main()
