# I parse all my friends and scan whether there are more male or female
import vk_api


def captcha_handler(captcha):
    """ При возникновении капчи вызывается эта функция и ей передается объект
        капчи. Через метод get_url можно получить ссылку на изображение.
        Через метод try_again можно попытаться отправить запрос с кодом капчи
    """

    key = input("Enter captcha code {0}: ".format(captcha.get_url())).strip()

    # Пробуем снова отправить запрос с капчей
    return captcha.try_again(key)

def main():
    login, password = '', ''
    vk_session = vk_api.VkApi(login, password, captcha_handler=captcha_handler)
    try: vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    vk = vk_session.get_api()
    friends = vk.friends.get(fields='sex')  # sex 1 stands for female and 2 for male
    count_male = count_female = 0
    print(friends)
    for friend in range(friends['count']):
        if friends['items'][friend]['sex'] == 1: count_female += 1
        else: count_male += 1
    print('{} у тебя девушек в друзьях и {} мальчиков'.format(count_female, count_male))

    # now we are going to check ratio between recent friends
    # my point is since November i add more girls than guys
    # so i think about main ratio...
    friends = vk.friends.getRecent(count=89)  # since September new friends
    count_male = count_female = 0
    print(friends)
    for friend in friends:
        recent_friend = vk.users.get(user_ids=friend, fields='sex')
        if recent_friend[0]['sex'] == 1: count_female += 1
        else: count_male += 1
    print('{} недавно добавленных девочек и мальчиков {}'.format(count_female, count_male))

if __name__ == '__main__':
    main()
