# Программа берет аудиозаписи 2 пользователей вк и рассчитывает между ними корреляцию
# То есть насколько у них совпадают аудио

import vk_api
from vk_api.audio import VkAudio
from login import *
from Levenshtein import ratio
from copy import deepcopy


def captcha_handler(captcha):
    """ При возникновении капчи вызывается эта функция и ей передается объект
        капчи. Через метод get_url можно получить ссылку на изображение.
        Через метод try_again можно попытаться отправить запрос с кодом капчи
    """

    key = input("Enter captcha code {0}: ".format(captcha.get_url())).strip()

    # Пробуем снова отправить запрос с капчей
    return captcha.try_again(key)


def main():
    vk_session = vk_api.VkApi(login, password)
    # vk_session = vk_api.VkApi(token=token)
    try: vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    # vk = vk_session.get_api()

    id1 = 202812518
    id2 = 172208616  # пользователи
    songs1, songs2 = [], []
    playlist1, playlist2, playlist3, playlist4, playlist5 = 4, 11, 5, 6, 2
    songs1_list, songs2_list = [], []

    vkaudio = VkAudio(vk_session)  # служебный метод для работы с аудио

    songs2.extend(vkaudio.get(owner_id=id2))
    songs1.extend(vkaudio.get(owner_id=id1))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist1))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist2))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist3))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist4))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist5))  # сбор песен

    for song in songs1:
        del song['id']
        del song['owner_id']
        del song['url']
        songs1_list.append((str(song['artist']).lower(), str(song['title']).lower(), song['duration']))
        # удаляем ненужную информацию из словарей песен

    for song in songs2:
        del song['id']
        del song['owner_id']
        del song['url']
        songs2_list.append((str(song['artist']).lower(), str(song['title']).lower(), song['duration']))

    songs1_list = sorted(list(set(songs1_list)))
    songs2_list = sorted(list(set(songs2_list)))  # сортируем и избавляемся от дубликатов

    t = 0
    similar = []
    # cmp1, cmp2 = deepcopy(songs1_list), deepcopy(songs2_list)
    for song in songs1_list:
        for check in songs2_list:

            # обработка данных для более точного определения совпадений без ложных срабатываний
            if check[0] in check[1]: check[1][len(check[0])+2:]
            if song[0] in song[1]: song[1][len(song[0])+2:]  # избавление от случаев, когда
            # артиста указывают в названии песни
            check[1].replace(check[1][check[1].rfind('['):check[1].rfind(']')+1], '')
            song[1].replace(song[1][song[1].rfind('['):song[1].rfind(']')+1], '')  # этим мы избавляемся
            # От индивижуальных приписок сообществ типа Рифмы и панчи
            # print(check)

            if ratio(check[0]+check[1], song[0]+song[1]) > 0.79 and abs(check[2] - song[2]) <= 2:
                t += 1
                # cmp1.remove(song)
                # cmp2.remove(check)
                similar.append(song)
    # songs1_list = deepcopy(cmp1)
    # songs2_list = deepcopy(cmp2)
    print(t)
    # print(similar)
    for song in similar:
        song = list(song)
        song[0] = song[0].title()
        song[1] = song[1].title()
        print(song)


main()
