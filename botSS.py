# version for keyboard with exact chats IMPORTANT
# Vk longpoll bot for studsovet
# Bot takes private messages from admins of Studsovet community in Vk
# If new message from admin, not random human
# Bot forwards that message to chats in which it is
import vk_api
from time import sleep  # avoid too many requests to vk api
from random import random
import json  # dict with chats id and names in json format
import requests
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType  # Separate methods for longpoll bot
from vk_api.utils import get_random_id  # required argument for messages.send
from bot import *  # All confidential info including passwords logins etc in file bot.py
import keyboards as kb
import conversations


def main():
    where_send = ['0', '0']
    with open('chats.json', 'r') as f:
        chats = json.load(f)  # loads from json list of conv where bot is (kinda database)

    vk_session = vk_api.VkApiGroup(token=token)  # Log in
    # app_id was stolen from Kate mobile app
    # because Vk has restricted messaging API for non-authorized bots
    vk = vk_session.get_api()

    while True:
        longpoll = VkBotLongPoll(vk=vk_session, group_id=group_id)  # creating longpoll bot
        print('ready')  # print in console to provide better understability

        try:  # vk api catches read timeout every night and this server bot raised exception
            for event in longpoll.listen():  # listening event from longpoll server in infinite cycle
                if event.type == VkBotEventType.MESSAGE_NEW and event.from_user and event.obj['message']['peer_id'] == creators[0] and event.obj['message']['text'].startswith('/updates'):
                    for admin in admins:
                        vk.messages.send(random_id=get_random_id(), peer_id=admin,
                                         group_id=group_id, message=event.obj['message']['text'])



                # if message from admin
                if event.type == VkBotEventType.MESSAGE_NEW and event.from_user and event.obj['message'][
                   'peer_id'] in admins + creators:
                    if event.obj['message']['text'] in ['/admin', 'At the beginning']:
                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                         group_id=group_id, message='Enjoy', keyboard=kb.keyboardAdmin.get_keyboard())
                    elif event.obj['message']['text'] == 'Mail to exact chat':
                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                         group_id=group_id, message='Type last 2 digits of chat ' + '\n\n'.join(key + ' chat: ' + str(val) for key, val in list(chats.items())),
                                         keyboard=kb.keyboardAdminExactChat.get_keyboard())
                    elif event.obj['message']['text'].isdigit():
                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                         group_id=group_id, message='Type your mailing')
                        where_send = ['chats', 2000000000 + int(event.obj['message']['text'])]
                    elif event.obj['message']['text'] == 'Conversations':
                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                         group_id=group_id, message='Choose where to send',
                                         keyboard=kb.keyboardAdminChats.get_keyboard())
                    elif event.obj['message']['text'] in list(conversations.ans.keys()) + ['All']:
                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                         group_id=group_id, message='Type your mailing')
                        where_send = ['chats', event.obj['message']['text']]

                    elif where_send != ['0', '0']:
                        #  Attachements
                        text = event.obj['message']['text']  # msg text from admin
                        attachments = event.obj['message']['attachments']  # list of attachments
                        attach = []
                        for att in attachments:
                            tip = att['type']  # type of attach
                            try:
                                owner_id = att[tip]['owner_id']
                            except KeyError:
                                owner_id = att[tip]['from_id']
                            media_id = att[tip]['id']
                            try:
                                access_key = att[tip][
                                    'access_key']  # access key is needed for access to non-mine attachments
                                attach.append('{}{}_{}_{}'.format(tip, owner_id, media_id, access_key))
                            except KeyError:  # in case particular attach has no access key
                                attach.append('{}{}_{}'.format(tip, owner_id, media_id))

                        if where_send[0] == 'chats':
                            if where_send[1] == 'All':
                                paths = list(chats.values())
                            elif isinstance(where_send[1], int):
                                paths = [where_send[1]]
                            else:
                                paths = []
                                for val in conversations.ans[where_send[1]]:
                                    paths.append(list(val.values())[0])
                        where_send = ['0', '0']

                        for path in paths:
                            try:  # in case of errors, for example when bot is removed from conf, that conf id still in list
                                vk.messages.send(random_id=get_random_id(), peer_id=path,
                                                 group_id=group_id, message=text, attachment=','.join(s for s in attach))
                            except (vk_api.exceptions.ApiError, IndexError):
                                sleep(1)
                                vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                                 group_id=group_id, message='A crash occured,'
                                                                            'sending failed to chat or user {},'
                                                                            'maybe bot has been deleted from this chat or '
                                                                            'user restrcited group to send him messages'.format(path))
                                continue

                        sleep(0.5)
                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                         group_id=group_id, message='Done for {} chats'.format(len(paths)), keyboard=kb.keyboardAdmin.get_keyboard())

                    elif where_send == ['0', '0']:
                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                         group_id=group_id, message='Wrong command', keyboard=kb.keyboardAdmin.get_keyboard())


                if event.type == VkBotEventType.MESSAGE_NEW and event.from_chat:  # if message from chat
                    if event.obj['message']['peer_id'] not in list(chats.values()):  # if it's new chat

                        chats.update({str(random()): event.obj['message']['peer_id']})  # and add to global variable

                        with open('chats.json', 'w') as file_json:  # add its id to separate txt file
                            # For saving (small db hahaha)
                            json.dump(chats, file_json)

                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'],
                                         group_id=group_id, message='Bot activated')  # More user friendly

                        vk.messages.send(random_id=get_random_id(), peer_id=creators[0], group_id=group_id,
                                         message='Bot has been added to new chat {}'.format(event.obj['message']['peer_id']))  # for a debug messages to me

                    # when bot is tagged inside chat we print little helper
                    elif event.obj['message']['peer_id'] in list(chats.values()) and event.obj['message']['text'] == '[club135324713|@studsovetgsb] help':
                        vk.messages.send(random_id=get_random_id(), peer_id=event.obj['message']['peer_id'], group_id=group_id,
                                         message="Hello, I'm not a chat bot (at least for now). My function is deliver newsletter from Studsovet GSB to dozens of VK chats across entire faculty. I promise no spam, only most important")
                        vk.messages.send(random_id=get_random_id(), peer_id=creators[0],
                                         group_id=group_id, message='There was tagging in chat {}'.format(event.obj['message']['peer_id']))
                        # Bot notifies me about every tagging


        except requests.exceptions.RequestException:
            print('catch read timeout, re auth to vk servers...')  # It's happening every night
            sleep(4)
            vk.messages.send(random_id=get_random_id(), peer_id=creators[0],
                             group_id=group_id, message='Catch read timeot, check server')
            continue


main()
