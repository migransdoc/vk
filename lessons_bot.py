# Этого бота надо добавить в беседу группы и он должен по запросу отсчитывает время до конца пары


import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
import time, datetime
from bot import *
from random import randint


def main():
    ends = [10 * 60 + 20, 11 * 60 + 50, 13 * 60 + 30, 15 * 60 + 0, 16 * 60 + 30, 18 * 60 + 0]

    vk_session = vk_api.VkApi(token=token)  # входим со стороны сообщества

    longpoll = VkLongPoll(vk_session)
    vk = vk_session.get_api()

    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            print(1)
            if event.from_chat:
                text = event.text.lower()  # сохраняем текст сообщения в строчных буквах
                weekday = datetime.date.weekday(datetime.date.today())  # день недели
                date = datetime.datetime.today()
                time = [date.hour, date.minute]  # текущее время в формате часы - минуты

                # слушаем лонгполл, если пришло сообщение, то обратбатаывем
                if text == 'поскорее бы эта срань закончилась' or \
                        text == 'сколько еще осталось' or text == 'сколько до конца':  # если заданная фраза
                    if weekday == 5 or weekday == 6:  # если сейчас выходные
                        vk.messages.send(peer_id=event.user_id, random_id=randint(1, 2 ** 32),
                                         message='ты дебзь в выходные на парах сидеть? пиздуй домой')
                    else:
                        if time[0] >= 18:  # если уже вечер
                            vk.messages.send(peer_id=event.user_id, random_id=randint(1, 2 ** 32),
                                             message='ты дурачок так поздно на парах сидеть? пиздуй домой')
                        elif time[0] < 9:  # если пары еще не начались
                            vk.messages.send(peer_id=event.user_id, random_id=randint(1, 2 ** 32),
                                             message='пары даже не начались еще чувак, удачи сдохнуть сегодня')
                        elif (time[0] == 10 and 21 <= time[1] <= 29) or (time[0] == 11 and 51 <= time[1] <= 59) or (
                            time[0] == 12 and 0 <= time[1] <= 9) or (time[0] == 13 and 31 <= time[1] <= 39) or (
                            time[0] == 15 and 0 <= time[1] <= 9) or (time[0] == 16 and 31 <= time[1] <= 39):  # перемена
                            vk.messages.send(chat_id=event.user_id, random_id=randint(1, 2 ** 32),
                                             message='чуитс детка, ща перемены, пойди отдохни')
                        else:  # обработали исключения, теперь по основному
                            time = time[0] * 60 + time[1]  # так удобней работать
                            for end in ends:
                                if time > end:
                                    continue  # пробегаемся по списку с концами пар
                                else:
                                    till = end - time  # нашли нашу пару и оставшееся время до конца
                                    break
                            if till >= 60:
                                vk.messages.send(peer_id=event.user_id, random_id=randint(1, 2 ** 32),
                                                 message='дорогой еще больше часа, держись')
                            elif 31 <= till <= 60:
                                vk.messages.send(peer_id=event.user_id, random_id=randint(1, 2 ** 32),
                                                 message='до конца пары весьма дохуя, лучше не буду тебя расстраивать')
                            else:
                                vk.messages.send(peer_id=event.chat_id, random_id=randint(1, 2 ** 32),
                                                 message='Держись братка, до конца {} минут'.format(till))


main()
