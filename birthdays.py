# this file gets my friend's link
# and parse through all of his friends to get their birthdays

import vk_api


def captcha_handler(captcha):
    """ При возникновении капчи вызывается эта функция и ей передается объект
        капчи. Через метод get_url можно получить ссылку на изображение.
        Через метод try_again можно попытаться отправить запрос с кодом капчи
    """

    key = input("Enter captcha code {0}: ".format(captcha.get_url())).strip()

    # Пробуем снова отправить запрос с капчей
    return captcha.try_again(key)


def main():
    login, password = '', ''
    vk_session = vk_api.VkApi(login, password, captcha_handler=captcha_handler)
    try: vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    vk = vk_session.get_api()

    friend = 'https://vk.com/id146095762'
    friend_id = '146095762'
    # friend = input()
    friends = vk.friends.get(user_id=friend_id, fields='bdate')  # get all friends with birthday date
    friends = friends['items']
    removing = []
    for friend in friends:
        del friend['online']
        del friend['id']  # удаляем ненужные поля из словарей
        if friend.get('bdate') is None: removing.append(friend)  # если у юзера нет др, удаляем его
    for removed in removing:
        friends.remove(removed)  # вот тут
    print(len(friends))
    friends.sort(key=lambda x: x['bdate'][x['bdate'].find('.')+1])  # сортируем по месяцу др
    for friend in friends:
        print(friend)


if __name__ == '__main__':
    main()
