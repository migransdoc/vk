# Надо было из обсуждения вытащить все песни, которые заказывали на ап
# И понять, какие у меня уже есть, а каких еще нет

import vk_api
from vk_api.audio import VkAudio
from login import *
from Levenshtein import ratio


def captcha_handler(captcha):
    """ При возникновении капчи вызывается эта функция и ей передается объект
        капчи. Через метод get_url можно получить ссылку на изображение.
        Через метод try_again можно попытаться отправить запрос с кодом капчи
    """

    key = input("Enter captcha code {0}: ".format(captcha.get_url())).strip()

    # Пробуем снова отправить запрос с капчей
    return captcha.try_again(key)


def main():
    vk_session = vk_api.VkApi(login, password)
    # vk_session = vk_api.VkApi(token=token)
    try: vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    vk = vk_session.get_api()

    audios = vk.board.getComments(group_id=177267969, topic_id=39534664, count=100)  # Получаем комменты из обсуждения
    messages = []
    for msg in audios['items']:
        try:
            messages.append(msg['attachments'])  # Ищем в каждом комменте аттачменты
        except KeyError: continue
    audios = []
    for message in messages:
        for audio in message:
            audios.append([audio['audio']['artist'].lower(), audio['audio']['title'].lower()])  # парсим инфу про песни
    # На этом этапе у нас в списке сохранены названия и исполнители всех песен из обсуждения

    id1 = 202812518
    songs1 = []
    playlist1, playlist2, playlist3, playlist4, playlist5 = 4, 11, 5, 6, 2
    songs1_list = []
    vkaudio = VkAudio(vk_session)  # служебный метод для работы с аудио
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist1))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist2))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist3))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist4))
    songs1.extend(vkaudio.get(owner_id=id1, album_id=playlist5))  # сбор моих песен

    for song in songs1:
        songs1_list.append((str(song['artist']).lower(), str(song['title']).lower()))
        # удаляем ненужную информацию из словарей песен

    songs1_list = sorted(list(set(songs1_list)))

    sim = 0  # Начинаем проверять похожие и непохожие
    unsim = 0
    similar = []
    unsimilar = []
    for check in audios:
        for song in songs1_list:

            # обработка данных для более точного определения совпадений без ложных срабатываний
            if check[0] in check[1]: check[1][len(check[0]) + 2:]
            if song[0] in song[1]: song[1][len(song[0]) + 2:]  # избавление от случаев, когда
            # артиста указывают в названии песни
            check[1].replace(check[1][check[1].rfind('['):check[1].rfind(']') + 1], '')
            song[1].replace(song[1][song[1].rfind('['):song[1].rfind(']') + 1], '')  # этим мы избавляемся
            # От индивижуальных приписок сообществ типа Рифмы и панчи
            # print(check)

            if ratio(check[0] + check[1], song[0] + song[1]) > 0.79:
                sim += 1
                similar.append(check)
                break
        else:
            unsim += 1
            unsimilar.append(check)

    print('similar ', sim)
    print('unsimilar ', unsim)
    for song in similar:
        song = list(song)
        song[0] = song[0].title()
        song[1] = song[1].title()
        print(song)
    print()
    for song in unsimilar:
        song = list(song)
        song[0] = song[0].title()
        song[1] = song[1].title()
        print(song)


main()
